import React, { useState } from "react";
import { useQuery } from "react-query";
import Pagination from "rc-pagination";
import "../node_modules/rc-pagination/assets/index.css";
import "./App.css";

const endpoint = "https://sites-pro-scholars.api.oit.duke.edu/graphiql/";

const PUBLICATIONS_QUERY = `
{
  publications(
    filter: [
      {field: PUBLICATION_YEAR, value: ["2022", "2021", "2020"]},
      {field: PRIMARY_ORG_ID, value: ["50000761"]},
    ],
    pageSize: 2000,
    startPage: 1,
    sort: {field: PUBLICATION_DATE, direction: DESC}
  ) {
    count
    results {
      id
      citations {
        html
        style
      }
    }
  }
}
`;

function App() {
  const [perPage, setPerPage] = useState(10);
  const [size, setSize] = useState(perPage);
  const [current, setCurrent] = useState(1);

  const { data, isLoading, error } = useQuery("publications", () => {
    return fetch(endpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ query: PUBLICATIONS_QUERY }),
    })
      .then((response) => {
        if (response.status >= 400) {
          throw new Error("Error fetching data");
        } else {
          return response.json();
        }
      })
      .then((data) => data.data);
  });

  if (isLoading) return "Loading...";
  if (error) return <pre>{error.message}</pre>;

  let formatted_publications = [];
  data.publications?.results?.forEach((pub, index) => {
    let citation = pub.citations?.filter(
      (cite) => cite.style === "chicago-fullnote-bibliography"
    );
    formatted_publications[index] = {
      id: pub.id,
      body: citation[0]?.html,
    };
  });

  const PerPageChange = (value) => {
    setSize(value);
    const newPerPage = Math.ceil(formatted_publications.length / value);
    if (current > newPerPage) {
      setCurrent(newPerPage);
    }
  };

  const getData = (current, pageSize) => {
    return formatted_publications.slice(
      (current - 1) * pageSize,
      current * pageSize
    );
  };

  const PaginationChange = (page, pageSize) => {
    setCurrent(page);
    setSize(pageSize);
  };

  return (
    <div>
      <h1>Test Publications</h1>
      <p>Total publications count: {data.publications.count}</p>
      <ul>
        {getData(current, size).map((pub) => {
          return (
            <li key={pub.id}>
              <div dangerouslySetInnerHTML={{ __html: pub.body }} />
            </li>
          );
        })}
      </ul>
      <Pagination
        className="pagination-data"
        onChange={PaginationChange}
        total={formatted_publications.length}
        current={current}
        pageSize={size}
        showSizeChanger={false}
        onShowSizeChange={PerPageChange}
      />
    </div>
  );
}

export default App;
