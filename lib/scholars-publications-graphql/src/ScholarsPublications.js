import React, { useState } from "react";
import { useQuery } from "react-query";
import Pagination from "rc-pagination";
import "../node_modules/rc-pagination/assets/index.css";

const endpoint = "https://sites-pro-scholars.api.oit.duke.edu/graphiql/";

const ScholarsPublications = ({
  itemsPerPage,
  orgID,
  onlyPrimary,
  timeline,
  idType,
}) => {
  const perPage = itemsPerPage > 0 ? itemsPerPage : 2000;
  const paginate = itemsPerPage > 0 ? true : false;
  const [size, setSize] = useState(perPage);
  const [current, setCurrent] = useState(1);

  const today = new Date();
  const currentYear = today.getFullYear();
  let pubYears = [];

  switch (timeline) {
    case "1":
      pubYears.push(`"${currentYear}"`);
      break;
    case "2":
      pubYears.push(
        `"${currentYear}"`,
        `"${currentYear - 1}"`
      );
      break;
    case "3":
      pubYears.push(
        `"${currentYear}"`,
        `"${currentYear - 1}"`,
        `"${currentYear - 2}"`
      );
      break;
    case "5":
      pubYears.push(
        `"${currentYear}"`,
        `"${currentYear - 1}"`,
        `"${currentYear - 2}"`,
        `"${currentYear - 3}"`,
        `"${currentYear - 4}"`,
      );
      break;
    case "all":
      pubYears.push("0");
      break;
    default:
      break;
  }

  const yearFilter = (pubYears[0] === '0') ?  '' : `{field: PUBLICATION_YEAR, value: [${pubYears}]},`
  const orgArrayQuoted = orgID.split(",").map((org) => `"${org}"`)
  const orgField = (idType === 'duid') ? "DUID" : (onlyPrimary === '1') ? "PRIMARY_ORG_ID" : "ANY_ORG_ID"

  const PUBLICATIONS_QUERY = `
  {
    publications(
      filter: [
        ${yearFilter}
        {field: ${orgField}, value: [${orgArrayQuoted}]},
      ],
      pageSize: 2000,
      startPage: 1,
      sort: {field: PUBLICATION_DATE, direction: DESC}
    ) {
      count
      results {
        id
        citations {
          html
          style
        }
      }
    }
  }
  `;

  const { data, isLoading, error } = useQuery("publications", () => {
    return fetch(endpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ query: PUBLICATIONS_QUERY }),
    })
      .then((response) => {
        if (response.status >= 400) {
          throw new Error("Error fetching data");
        } else {
          return response.json();
        }
      })
      .then((data) => data.data);
  });

  if (isLoading) return "Loading...";
  if (error) return <pre>{error.message}</pre>;

  let formatted_publications = [];
  data.publications?.results?.forEach((pub, index) => {
    let citation = pub.citations?.filter(
      (cite) => cite.style === "chicago-fullnote-bibliography"
    );
    formatted_publications[index] = {
      id: pub.id,
      body: citation[0]?.html,
    };
  });

  const PerPageChange = (value) => {
    setSize(value);
    const newPerPage = Math.ceil(formatted_publications.length / value);
    if (current > newPerPage) {
      setCurrent(newPerPage);
    }
  };

  const getData = (current, pageSize) => {
    return formatted_publications.slice(
      (current - 1) * pageSize,
      current * pageSize
    );
  };

  const PaginationChange = (page, pageSize) => {
    setCurrent(page);
    setSize(pageSize);
  };

  return (
    <div>
      <ul>
        {getData(current, size).map((pub) => {
          return (
            <li key={pub.id}>
              <div dangerouslySetInnerHTML={{ __html: pub.body }} />
            </li>
          );
        })}
      </ul>
      {paginate && formatted_publications.length > 0 && (
        <Pagination
          className="pagination-data"
          onChange={PaginationChange}
          total={formatted_publications.length}
          current={current}
          pageSize={size}
          showSizeChanger={false}
          onShowSizeChange={PerPageChange}
        />
      )}
    </div>
  );
};

export default ScholarsPublications;
