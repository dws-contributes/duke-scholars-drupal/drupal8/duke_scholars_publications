import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import ScholarsPublications from './ScholarsPublications';
import { QueryClient, QueryClientProvider } from "react-query";

const client = new QueryClient();

// Use for deployment in Scholars Publications Drupal module
const container = document.querySelector('#scholars-publications-wrapper');
const root = ReactDOM.createRoot(container);
root.render(
  <QueryClientProvider client={client}>
    <ScholarsPublications
      itemsPerPage = {container?.getAttribute('perPage') || 10}
      orgID = {container?.getAttribute('orgID') || '0'}
      onlyPrimary = {container?.getAttribute('primary') || false}
      timeline = {container?.getAttribute('timeline') || '1'}
      idType = {container?.getAttribute('idtype') || 'org'}
    />
  </QueryClientProvider>
);

// Use for local testing
// const container = document.querySelector('#scholars-publications-wrapper');
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <QueryClientProvider client={client}>
//     <ScholarsPublications
//       itemsPerPage = {container?.getAttribute('perPage') || '0'}
//       orgID = {container?.getAttribute('orgID') || '0025934'}
//       onlyPrimary = {container?.getAttribute('primary') || true}
//       timeline = {container?.getAttribute('timeline') || 'all'}
//       idType = {container?.getAttribute('idType') || 'duid'}
//     />
//   </QueryClientProvider>
// );
