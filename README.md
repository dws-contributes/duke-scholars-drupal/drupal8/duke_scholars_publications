# Duke Scholars Publications

This custom Drupal module adds a paragraph bundle named 'Publication List' to allow customazible display of publications from GraphQL Scholars api.

The following fields are installed:
- <b>Administrative title</b> - only displays on the admin side of your site.
- <b>Display title</b> - displays on the front-end of your site.
- <b>Primary Appointments Only</b> - if checked, the list will display only publications where one or more authors have a primary appointment in the unit.
- <b>Organization ID</b> - your unit's Organization ID from Scholars@Duke. Multiple values are accepted.
- <b>Timeframe</b> - options: last 2 years, last 3 years, last 5 years, all.
- <b>Number of items per page</b> - the number of publications that will appear on each page. If 0 is selected, all publications in your timeframe will appear without pagination.

The module utilizes the React app from `lib/scholars-publications-graphql` to display the list of publications in real time in the default paragraph display. 
For more information about the React app, see `lib/scholars-publications-graphql/README.md`.


## How to install this module
- add the following to your composer.json file: 
    ```
    "repositories": {    
        "duke/duke_scholars_publications": {
            "type": "vcs",
            "url": "https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke_scholars_publications.git"
        },
        ...
    }    
    ```
- add dependency: 
    ```
    composer require duke/duke_scholars_publications
    ```
- enable the module:
    ```
    drush en duke_scholars_publications
    ```

### How to use this module
Add a field of type `Entity reference revisions` to your content type. Select Publication List as one of the paragraph types.

### Dependencies
- [Paragraphs](https://www.drupal.org/project/paragraphs)
- Options, Paragraphs, Entity Reference Revisions, Field, File